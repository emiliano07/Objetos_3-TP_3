package microcontroller

import java.util.ArrayList
import java.util.function.Consumer
import scala.collection.mutable.ArrayBuffer

abstract class Instruction {
  def execute(microcontroller : Microcontroller)
}

class NOP() extends Instruction{
  def execute(microcontroller : Microcontroller) {
  //No�operaci�n,�el�programa�sigue�en�la�pr�xima�instrucci�n.
  }
}

class ADD() extends Instruction{
  def execute(microcontroller : Microcontroller) {
  //Suma�los�valores�de�los�dos�acumuladores.
    if(microcontroller.a.toInt + microcontroller.b.toInt < 127){
      microcontroller.b = (microcontroller.a.toInt + microcontroller.b.toInt).toByte
      microcontroller.a = 0.toByte
    }
    else{
      microcontroller.a = (microcontroller.a.toInt + microcontroller.b.toInt - 127).toByte
      microcontroller.b = 127.toByte
    }
  }
}

class SUB() extends Instruction{
  def execute(microcontroller : Microcontroller) {
  //Resta�el�valor�del�acumulador�A�al�valor�del�acumulador�B.
    if(microcontroller.a.toInt - microcontroller.b.toInt < 127){
      microcontroller.b = (microcontroller.a.toInt - microcontroller.b.toInt).toByte
      microcontroller.a = 0.toByte
    }
    else{
      microcontroller.a = (microcontroller.a.toInt - microcontroller.b.toInt - 127).toByte
      microcontroller.b = 127.toByte
    }
  }
}

class DIV() extends Instruction{
  def execute(microcontroller : Microcontroller) {
  //Divide�el�valor�del�acumulador�A�por�el�valor�del�acumulador�B.
    if(microcontroller.b == 0){
      microcontroller.stop()
      throw new Exception("Division por cero")
    }
    else
      if(microcontroller.a.toInt / microcontroller.b.toInt < 127){
      microcontroller.b = (microcontroller.a.toInt / microcontroller.b.toInt).toByte
      microcontroller.a = 0.toByte
    }
    else{
      microcontroller.a = (microcontroller.a.toInt / microcontroller.b.toInt - 127).toByte
      microcontroller.b = 127.toByte
    }
  }
}

class SWAP() extends Instruction{
  def execute(microcontroller : Microcontroller) {
  //Intercambia�los�valores�de�los�acumuladores
    var aAux : Byte = microcontroller.a
    microcontroller.a = microcontroller.b
    microcontroller.b = aAux
  }
}

class LOD(addr : Int) extends Instruction{
  def execute(microcontroller : Microcontroller) {
  //Carga el acumulador A con el contenido de la memoria de datos en la posicion�addr.
    if(microcontroller.memory.legalAddress(addr))
      microcontroller.a = microcontroller.memory.get(addr)
    else
      throw new IllegalArgumentException
  }
}

class STR(addr : Int) extends Instruction{
  def execute(microcontroller : Microcontroller) {
  //Guarda el valor del acumulador A en la posicion addr de la memoria de datos.
    if(microcontroller.memory.legalAddress(addr))
      microcontroller.memory.save(microcontroller.a, addr)
    else
      throw new IllegalArgumentException
  }
}

class LODV(value : Byte) extends Instruction{
  def execute(microcontroller : Microcontroller) {
  //Carga�en�el�acumulador�A�el�valor�value.
    microcontroller.a = value
  }
}

class HALT() extends Instruction{
  def execute(microcontroller : Microcontroller) {
  //Detiene la ejecuci�n del programa, cualquier operaci�n posterior debe arrojar excepci�n�de�negocio.
    microcontroller.stop()
  }
}

class WHNZ(instructions : ArrayList[Instruction]) extends Instruction{
  def execute(microcontroller : Microcontroller) {
  //Ejecuta un conjunto de instrucciones mientras el valor del acumulador A sea distinto de cero.
    var ins = 0
    while(microcontroller.a != 0){
      if(instructions.size() == ins)
        throw new Exception("La ejecucion del programa fue detenida antes de finalizar")
      instructions.get(ins).execute(microcontroller)
      ins = ins + 1
     }
  }
}

class IFNZ(instructions : ArrayBuffer[Instruction]) extends Instruction{
  def execute(microcontroller : Microcontroller) {
  //Ejecuta un conjunto de instrucciones si el valor del acumulador A es distinto de cero.
    if(microcontroller.a != 0) 
       instructions.foreach { inst => inst.execute(microcontroller) }
   }
}