package microcontroller

import scala.collection.mutable.Stack
import scala.collection.mutable.Map

class Microcontroller(var memory : Memory) {
  
  var a : Byte = 0.toByte
  var b : Byte  = 0.toByte
  var pc : Int  = 0
  var program : Program = new NoProgram()
  var state : State = new Stopped()
  var image : Stack[Map[String, Byte]] = new Stack()
  
  def newImage(valorA : Byte, valorB : Byte){
    image.push(Map("a" -> valorA, "b" -> valorB))
  }
  
  def load(prog : Program){
    if(state.isStopped())
      program = prog
    else
     throw new Exception("El microprocesador no se encuentra detenido")
  }
  
  def start(){
    if(state.isStopped()){
      state = new Running()
      memory.resetMemory()
      restartPC()
    }
    else
     throw new Exception("El microprocesador no se encuentra detenido")
    if(program.isAProgram())
      program.execute(this)
    else
     throw new Exception("Debe haber un programa cargado")
  }
  
  def incrementPC(){
    pc = pc + 1
  }
  
  def restartPC(){
    pc = 0
  }
  
  def stop(){
    state = new Stopped()
  }
  
  def deleteLastInstruction(){
    var mapValues = image.pop()
    a = mapValues.get("a").get
    b = mapValues.get("b").get
  }
}