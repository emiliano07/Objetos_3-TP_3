package microcontroller

import scala.collection.mutable.ListBuffer
import scala.collection.mutable.ArrayBuffer
import java.util.ArrayList

class Program {
 
  var instructions : ArrayList[Instruction] = new ArrayList()
  
  def add(instruction : Instruction){
    instructions.add(instruction)
  }
  
  def execute(microcontroller: Microcontroller){
    while(! microcontroller.state.isStopped()){
      microcontroller.newImage(microcontroller.a, microcontroller.b)
      instructions.get(microcontroller.pc).execute(microcontroller)
      microcontroller.incrementPC()
      if(instructions.size() == microcontroller.pc)
        microcontroller.stop()
    }
    if(instructions.size() != microcontroller.pc)
        throw new Exception("La ejecucion del programa fue detenida antes de finalizar")
  }
  
  def isAProgram() : Boolean = true
}

class NoProgram() extends Program{
  override def isAProgram() : Boolean = false
}