package microcontroller

class Memory {
  
  var dates = Array.fill[Byte](1024)(0.toByte)
  
  def save(date : Byte, addr : Int){
    dates(addr) = date
  }
  
  def get(addr : Int) = {
    dates(addr)
  }
  
  def legalAddress(addr : Int) = {
    addr <= dates.length
  }
  
  def resetMemory(){
    dates = Array.fill[Byte](1024)(0.toByte)
  }
}