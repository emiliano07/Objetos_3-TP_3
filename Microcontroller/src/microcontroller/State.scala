package microcontroller

abstract class State {
  def isStopped() : Boolean
}

class Stopped extends State{
  def isStopped() = true
}

class Running extends State{
  def isStopped() = false
}