package microcontrollerTest

import org.junit.Assert
import org.junit.Before
import org.junit.Test

import microcontroller.ADD
import microcontroller.DIV
import microcontroller.LOD
import microcontroller.LODV
import microcontroller.Memory
import microcontroller.Microcontroller
import microcontroller.NOP
import microcontroller.Program
import microcontroller.STR
import microcontroller.SWAP

class BasicInstructionsTest {
  
  var memory = new Memory()
  var program = new Program()
  var microcontroller:Microcontroller = _
  
  @Before
  def setUp(){
    memory = new Memory()
    program = new Program()
    microcontroller = new Microcontroller(memory)
  }
 
  @Test
  def avanzar_3_posiciones(){
    program.add(new NOP())
    program.add(new NOP())
    program.add(new NOP())
    microcontroller.load(program)
    microcontroller.start()
    Assert.assertEquals(microcontroller.pc, 3)
  }
  
  @Test
  def suma_I(){
    program.add(new LODV(10.toByte))
    program.add(new SWAP())
    program.add(new LODV(22.toByte))
    program.add(new ADD())
    microcontroller.load(program)
    microcontroller.start()
    print(microcontroller.b)
    Assert.assertEquals(microcontroller.a.toInt, 0)
    Assert.assertEquals(microcontroller.b.toInt, 32)
  }
  
  @Test
  def suma_II(){
    program.add(new LODV(100.toByte))
    program.add(new SWAP())
    program.add(new LODV(50.toByte))
    program.add(new ADD())
    microcontroller.load(program)
    microcontroller.start()
    Assert.assertEquals(microcontroller.a.toInt, 23)
    Assert.assertEquals(microcontroller.b.toInt, 127)
  }
  
  @Test
  def suma_III(){
    program.add(new LODV(2.toByte))
    program.add(new STR(0))
    program.add(new LODV(8.toByte))
    program.add(new SWAP())
    program.add(new LODV(5.toByte))
    program.add(new ADD())
    program.add(new LOD(0))
    program.add(new ADD())
    microcontroller.load(program)
    microcontroller.start()
    Assert.assertEquals(microcontroller.a.toInt, 0)
    Assert.assertEquals(microcontroller.b.toInt, 15)
  }
  
  @Test
  def dividir_por_0(){
    program.add(new LODV(0.toByte))
    program.add(new SWAP())
    program.add(new LODV(2.toByte))
    program.add(new DIV())
    microcontroller.load(program)
    try{
      microcontroller.start()
    } 
    catch{
         case ex: Exception => {
            println(ex)
         }
    }
  }
}