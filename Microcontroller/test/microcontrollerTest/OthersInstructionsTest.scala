package microcontrollerTest

import microcontroller.Memory
import microcontroller.Microcontroller
import org.junit.Before
import microcontroller.Program
import org.junit.Test
import org.junit.Assert
import microcontroller.NOP
import microcontroller.ADD
import microcontroller.SWAP
import microcontroller.LODV
import microcontroller.HALT

class OthersInstructionTest {
   
  var memory = new Memory()
  var program = new Program()
  var microcontroller:Microcontroller = _
  
  @Before
  def setUp(){
    memory = new Memory()
    program = new Program()
    microcontroller = new Microcontroller(memory)
  }
 
  @Test
  def deleteLastInstruction(){
    program.add(new LODV(10.toByte)) //a = 10
    program.add(new SWAP())   // b -> 10
    program.add(new LODV(22.toByte)) // a = 22
    program.add(new ADD()) // a = 0, b = 32
    microcontroller.load(program)
    microcontroller.start()
    microcontroller.deleteLastInstruction()
    Assert.assertEquals(microcontroller.a.toInt, 22)
    Assert.assertEquals(microcontroller.b.toInt, 10)
   }
  
  @Test
  def halt(){
    program.add(new LODV(10.toByte)) //a = 10
    program.add(new SWAP())   // b -> 10
    program.add(new HALT())
    program.add(new LODV(22.toByte)) // a = 22
    program.add(new ADD()) // a = 0, b = 32
    microcontroller.load(program)
    try{
      microcontroller.start()
    } 
    catch{
         case ex: Exception => {
            println(ex)
         }
    }
    Assert.assertEquals(microcontroller.a.toInt, 0)
    Assert.assertEquals(microcontroller.b.toInt, 10)
  }
}